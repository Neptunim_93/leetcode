#include <iostream>
#include <string>
#include <unordered_map>
using namespace std;

class Solution {
public:
  int lengthOfLongestSubstring(string s) {
    int res = 0;
    unordered_map<char, int> map;
    for (int i = 0, j = 0; j < s.size(); j++) {
      if (map.find(s[j]) != map.end()) {
        cout << "if" << endl;
        cout << "map[s[j]] = " << map[s[j]] << endl;
        cout << "i = " << i << endl;
        cout << "j = " << j << endl;
        cout << "s[j] = " << s[j] << endl;
        i = max(map[s[j]], i);
      } else {
        cout << "else" << endl;
        cout << "map[s[j]] = " << map[s[j]] << endl;
        cout << "i = " << i << endl;
        cout << "j = " << j << endl;
        cout << "s[j] = " << s[j] << endl;
      }
      for (const auto &pair : map) {
        cout << pair.first << ": " << pair.second << " ";
      }
      cout << endl;
      cout << "##########" << endl;
      res = max(res, j - i + 1);
      map[s[j]] = j + 1;
    }
    cout << endl << endl;
    return res;
  }
};

int main() {
  Solution s;
  string s1 = "abcabcbb";
  string s2 = "bbbbb";
  string s3 = "pwwkew";
  cout << s.lengthOfLongestSubstring(s1) << endl;
  cout << s.lengthOfLongestSubstring(s2) << endl;
  cout << s.lengthOfLongestSubstring(s3) << endl;
  return 0;
}
